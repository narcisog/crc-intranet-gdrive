const router = require("express").Router();
const { google } = require("googleapis");
const fs = require("fs");
const readline = require("readline");
const multer = require("multer");
const stream = require("stream");
const http = require("http");
const https = require("https");
const url = require("url");
const open = require("open");
// const { rawListeners } = require("process");
const express = require("express");
const moment = require("moment");
const { OAuth2Client } = require("google-auth-library");

const SCOPES = [
  "https://www.googleapis.com/auth/drive",
  "https://www.googleapis.com/auth/userinfo.email",
  "https://www.googleapis.com/auth/userinfo.profile",
];
const client_id =
  "611873017973-04maitp5l9mrutflsuuee3m0c2i81cso.apps.googleusercontent.com";
const client_secret = "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";
// const redirect_uris = [
//   "https://senti-a-ialert-website-pd.df.r.appspot.com/main",
//   "http://localhost:80/main",
// ];
const redirect_uris = "http://localhost:80/main";
// const redirect_uris = "https://senti-a-ialert-website-pd.df.r.appspot.com/main";

// OAuth version 2 =========================================================

const client = new OAuth2Client(client_id);

const oAuth2Client = new google.auth.OAuth2(
  client_id,
  client_secret,
  redirect_uris
);

const drive = google.drive({
  version: "v3",
  auth: oAuth2Client,
});

const authorizationUrl = oAuth2Client.generateAuthUrl({
  access_type: "offline",
  scope: SCOPES,
  include_granted_scopes: true,
});

let userCredential = null;

// async function main() {
//   const server = http.createServer(async (req, res) => {
//     if (req.url == "/login") {
//       console.log("step 3", authorizationUrl);
//       res.writeHead(301, { Location: authorizationUrl });
//     }

//     if (req.url.startsWith("/main")) {
//       console.log("step 4");
//       let q = url.parse(req.url, true).query;

//       if (q.error) {
//         console.log("Error:" + q.error);
//       } else {
//         let { tokens } = await oAuth2Client.getToken(q.code);

//         var now = new Date();
//         var expiryDate = new Date(now.getTime() + tokens.expiry_date);
//         var expiryDate_moment = moment(expiryDate).format("YYYY-MM-DD");

//         oAuth2Client.setCredentials(tokens);

//         userCredential = tokens;

//         res.writeHead(301, {
//           Location: "http://localhost:3000/main",
//           // Location: "https://git.heroku.com/crc-intranet.git/main",
//         });
//       }
//     }

//     if (req.url == "/revoke") {
//       // Build the string for the POST request
//       let postData = "token=" + userCredential.access_token;

//       // Options for POST request to Google's OAuth 2.0 server to revoke a token
//       let postOptions = {
//         host: "oauth2.googleapis.com",
//         port: "443",
//         path: "/revoke",
//         method: "POST",
//         headers: {
//           "Content-Type": "application/x-www-form-urlencoded",
//           "Content-Length": Buffer.byteLength(postData),
//         },
//       };
//       S;

//       // Set up the request
//       const postReq = https.request(postOptions, function(res) {
//         res.setEncoding("utf8");
//         res.on("data", (d) => {
//           console.log("Response: " + d);
//         });
//       });

//       postReq.on("error", (error) => {
//         console.log(error);
//       });

//       // Post the request with data
//       postReq.write(postData);
//       postReq.end();
//     }
//     res.end();
//   });

//   server.listen(80, () => console.log("Running on port 80!"));
// }

// main().catch((err) => console.log(err));
// OAuth version 2 =========================================================

const upload = multer();

const uploadFile = async (fileObject, fID, permissions, dom) => {
  const bufferStream = new stream.PassThrough();
  bufferStream.end(fileObject.buffer);

  if (fID !== "") {
    const { data } = await drive.files.create({
      media: {
        mimeType: fileObject.mimeType,
        body: bufferStream,
      },
      requestBody: {
        name: fileObject.originalname,
        parents: [fID],
        properties: {
          domain: dom,
        },
      },
      fields: "id,name,properties",
    });
    console.log(
      `Uploaded file ${data.name} ${data.id} ${data.properties.domain} ${dom}`
    );
    if (permissions.length !== 0) {
      let id;
      try {
        permissions.forEach((permission) => {
          drive.permissions
            .create({
              resource: {
                type: permission.type,
                role: permission.role,
                emailAddress: permission.emailAddress,
              },
              fileId: data.id,
              fields: "id",
            })
            .then(function(result) {
              id = result.data.id;
              console.log("Permission Id: ", id);
            });
        });
        return id;
      } catch (err) {
        console.log(err.message);
      }
    }
  } else if (fID === "") {
    const { data } = await drive.files.create({
      media: {
        mimeType: fileObject.mimeType,
        body: bufferStream,
      },
      requestBody: {
        name: fileObject.originalname,
        properties: {
          domain: dom,
        },
      },
      fields: "id,name,properties",
    });
    console.log(
      `Uploaded file ${data.name} ${data.id} ${data.properties.domain} ${dom}`
    );
    if (permissions.length !== 0) {
      let id;
      try {
        permissions.forEach((permission) => {
          drive.permissions
            .create({
              resource: {
                type: permission.type,
                role: permission.role,
                emailAddress: permission.emailAddress,
              },
              fileId: data.id,
              fields: "id",
            })
            .then(function(result) {
              id = result.data.id;
              console.log("Permission Id: ", id);
            });
        });
        return id;
      } catch (err) {
        console.log(err.message);
      }
    }
  }
};

// get access token

router.route("/getAccessToken").get(async (req, res) => {
  console.log(req.url);

  const authorizationUrl = oAuth2Client.generateAuthUrl({
    access_type: "offline",
    scope: SCOPES,
    include_granted_scopes: true,
  });

  // console.log(authorizationUrl);
  // res.redirect(authorizationUrl);
  res.status(200).send(authorizationUrl);
  // res.writeHead(301, { Location: authorizationUrl });
});

// Post access token

router.route("/postToken").post(upload.any(), async (req, res) => {
  const { body } = req;
  let accessToken = JSON.parse(body["0"]);

  oAuth2Client.setCredentials(accessToken);
  res.status(200);
});

// Get access token

router.route("/getToken").get(async (req, res) => {
  if (userCredential !== null) {
    res.status(200).send(userCredential);
  }
});

// Create a folder

router.route("/create").get(async (req, res) => {
  const title = req.query.title;
  const fID = req.query.fID;

  if (fID === "") {
    const metadata = {
      name: title,
      mimeType: "application/vnd.google-apps.folder",
    };
    try {
      const folder = await drive.files.create({
        resource: metadata,
        fields: "id",
      });
      // console.log("Folder ID:", folder.data);
      res.status(200).json(folder.data);
    } catch (error) {
      console.log(error.message);
    }
  } else if (fID !== "") {
    console.log("Folder Details:", title, fID);
    try {
      const folder = await drive.files.create({
        resource: {
          name: title, //set folder name from request or variable
          mimeType: "application/vnd.google-apps.folder",
          parents: [fID],
        },
        fields: "id",
      });
      res.status(200).json(folder.data.id);
      // console.log("Folder ID:", folder.data.id);
    } catch (error) {
      console.log(error.message);
    }
  }
});

// Upload file

router.route("/upload").post(upload.any(), async (req, res) => {
  let idList = [];
  try {
    const { body, files } = req;
    const permissions = JSON.parse(body["p"]);
    const domain = body["domain"];
    // console.log("THIS ARE THE FILES", files);

    for (let f = 0; f < files.length; f += 1) {
      await uploadFile(files[f], body["0"], permissions, domain);
    }
    res.status(200).send(idList);
  } catch (error) {
    var errVal = 0;
    console.log("THIS IS THE ERROR MESSAGE", errVal);
    res.status(401).send();
  }
});

// delete file

router.route("/delete").delete(async (req, res) => {
  const fID = req.query.fID;

  try {
    await drive.files.delete({
      fileId: fID,
    });

    res.sendStatus(200);
  } catch (error) {
    console.log(error.message);
    res.status(401).send();
  }
});

// get ALL Files

router.route("/MyDrive").get(async (req, res) => {
  try {
    const result = await drive.files.list({
      q: "'root' in parents and trashed = false",
      fields:
        "files(kind,id,name,createdTime,modifiedTime,size,mimeType,permissions,properties)",
      supportsTeamDrives: true,
      supportsAllDrives: true,
      includeTeamDriveItems: true,
    });
    res.status(200).json(result.data.files);
    // console.log(result.data.files);
  } catch (error) {
    console.log(error.message);
  }
});

router.route("/Shared").get(async (req, res) => {
  try {
    const result = await drive.files.list({
      q: "sharedWithMe and trashed = false",
      fields:
        "files(kind,id,name,createdTime,modifiedTime,size,mimeType,permissions,properties)",
      supportsTeamDrives: true,
      supportsAllDrives: true,
      includeTeamDriveItems: true,
    });
    res.status(200).json(result.data.files);
    // console.log(result.data.files);
  } catch (error) {
    console.log("ERROR", error.message);
  }
});

router.route("/crc").get(async (req, res) => {
  try {
    const result = await drive.files.list({
      q: "properties has {key='domain' and value='conflictalert.info'}",
      fields:
        "files(kind,id,name,createdTime,modifiedTime,size,mimeType,permissions,properties)",
      // supportsTeamDrives: true,
      // supportsAllDrives: true,
      // includeTeamDriveItems: true,
    });
    // console.log(result.data.files);
    res.status(200).json(result.data.files);
  } catch (error) {
    console.log("ERROR", error.message);
  }
});

router.route("/folder").get(async (req, res) => {
  const fID = req.query.fID;
  // console.log("ID: ", req.query.fID);
  try {
    const result = await drive.files.list({
      q: `'${fID}' in parents`,
      fields:
        "files(kind,id,name,createdTime,modifiedTime,size,mimeType,permissions,properties)",
      supportsTeamDrives: true,
      supportsAllDrives: true,
      includeTeamDriveItems: true,
    });
    res.status(200).json(result.data.files);
    console.log(result.data.files);
  } catch (error) {
    console.log("error fetching data: ", error.message);
  }
});

router.route("/share").post(upload.any(), async (req, res) => {
  const { body } = req;
  const fID = body["0"];
  const permissions = JSON.parse(body["p"]);
  // console.log("File for sharing", body["0"]);
  let id;

  try {
    permissions.forEach((permission) => {
      drive.permissions
        .create({
          resource: permission,
          fileId: fID,
          fields: "id",
        })
        .then(function(result) {
          id = result.data.id;
          console.log("Permission Id: ", id);
        });
    });
    res.status(200).json(id);
    // return id;
  } catch (err) {
    console.log(err.message);
  }
});

// Get permissions
router.route("/getPermissions").get(async (req, res) => {
  const fID = req.query.fID;
  try {
    const results = await drive.permissions.list({
      fileId: fID,
      fields: "permissions(id,displayName,emailAddress,role,type)",
      supportsAllDrives: true,
    });
    res.status(200).json(results.data.permissions);
  } catch {}
});

router.route("/deletePermissions").delete(async (req, res) => {
  const fID = req.query.fID;
  const forDelete = JSON.parse(req.query.forDelete);
  console.log("FOR DELETION", forDelete);
  try {
    forDelete[0].forEach((item) => {
      drive.permissions
        .delete({
          fileId: fID,
          permissionId: item.id,
        })
        .then((res) => console.log(res));
      res.sendStatus(204);
    });
  } catch (err) {
    console.log(err.message);
  }
});

router.route("/updatePermissions").put(upload.any(), async (req, res) => {
  const { body } = req;
  const fID = body["0"];
  const forUpdate = JSON.parse(body["p"]);
  console.log(forUpdate);
  try {
    forUpdate.forEach((item) => {
      drive.permissions
        .get({
          fileId: fID,
          permissionId: item.id,
        })
        .then((res) => {
          res.role = item.role;

          drive.permissions.update({
            fileId: fID,
            permissionId: item.id,
            resource: res,
          });
        })
        .then((res) => console.log(res));
      res.sendStatus(200);
    });
  } catch (error) {}
});

//   async function getFolders() {
//     try {
//       const result = await drive.files.list({
//         q: "mimeType = 'application/vnd.google-apps.folder'", //get all folders
//         supportsAllDrives: true,
//       });

//       console.log(result.data.files);
//     } catch (error) {
//       console.log(error.message);
//     }
//   }

//   async function getFilesByFolder() {
//     try {
//       const result = await drive.files.list({
//         //   q: "mimeType = 'application/vnd.google-apps.folder'",
//         q: "'1t4NcPUN9meDrNhLUnaDzTRPvDBri5ssN''' in parents", //get files in folder with given folder ID
//         // fields:,
//       });

//       console.log(result.data.files);
//     } catch (error) {
//       console.log(error.message);
//     }
//   }

//   // create public URL of uploaded file

router.route("/generateURL").get(async (req, res) => {
  const fID = req.query.fID;
  console.log(fID);

  try {
    await drive.permissions.create({
      //sets or changes permission of the file with given fileID to public reader
      fileId: fID,
      requestBody: {
        role: "reader",
        type: "anyone",
      },
    });

    const result = await drive.files.get({
      fileId: fID,
      fields: "webViewLink, webContentLink", //webcontent link, automatically downloads the file; webView is only for viewing
    });
    res.status(200).send(result.data);
    console.log(result.data);
  } catch (error) {
    console.log(error.message);
  }
});

// uploadFile();
// deleteFile();
// generatePublicUrl();
//   getAll();

module.exports = router;
