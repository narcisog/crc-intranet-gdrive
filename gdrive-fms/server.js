const express = require("express");
const cors = require("cors");
const path = require("path");
const bodyParser = require("body-parser");

const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// establish server
var port = process.env.PORT || 8080;

// app.use(cors());
// app.use(express.static(path.join(__dirname, "build")));

app.use(function(req, res, next) {
  res.header(
    "Access-Control-Allow-Origin",
    // "https://senti-a-ialert-website-pd.df.r.appspot.com"
    "*"
  );
  res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

const gdriveRouter = require("./routes/gdrive");

app.use("/gdrive", gdriveRouter);

if (process.env.NODE_ENV === "production") {
  app.use(express.static(path.join(__dirname, "/client/build")));
  app.get("*", (req, res) => {
    req.sendfile(path.resolve(__dirname, "/client/build", "index.html"));
  });
}
app.listen(port, () => console.log(`Running on port ${port}!`));
