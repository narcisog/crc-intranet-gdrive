import React from "react";
import Login from "./components/login";
import Main from "./components/main";
import "./App.css";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";

// const Login = () => {
//   return (
//     <div>
//       <h2>Hello world! This is login page</h2>
//     </div>
//   );
// };

export default function App() {
  let isSignedIn = window.localStorage.getItem("isSignedIn");
  return (
    // <Login />
    <Router>
      <div>
        <Routes>
          <Route path="/" element={<Login />} />
          <Route
            path="/main"
            element={isSignedIn === "true" ? <Main /> : <Login />}
          />
        </Routes>
      </div>
    </Router>
  );
}
