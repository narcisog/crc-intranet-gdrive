import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import ContextProvider from "./contexts/contexts";

// const root = ReactDOM.createRoot(document.getElementById("root"));
ReactDOM.render(
  <React.StrictMode>
    <ContextProvider>
      <App />
    </ContextProvider>
  </React.StrictMode>,

  document.getElementById("root")
);

// class Root extends React.Component {
//   render() {
//     return (
//       <React.StrictMode>
//         <ContextProvider>
//           <App />
//         </ContextProvider>
//       </React.StrictMode>
//     );
//   }
// }

// export default Root;

// ReactDOM.render(<Root />, document.getElementById("root"));

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
