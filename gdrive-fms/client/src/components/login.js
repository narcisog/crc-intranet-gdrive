import React, { useContext } from "react";
import "./styles/components.css";
import axios from "axios";
import logo from "../assets/FP-icon-CA-logo@2x.png";

import { Context } from "../contexts/contexts";

export default function Login() {
  const { setFiles, fID } = useContext(Context);

  // const host = "http://localhost:8080";
  const host = "https://git.heroku.com/crc-intranet.git";
  const authUrl = "http://localhost:80/login";

  const handleSignIn = () => {
    // setIsSignedIn(!isSignedIn);
    console.log(fID);

    window.localStorage.setItem("isSignedIn", "true");

    let token = JSON.parse(window.localStorage.getItem("token"));
    console.log(token);

    if (token === "" || token === null || token === undefined) {
      console.log("LOGIN");

      window.open(authUrl, "_self");

      // const fetchData = async () => {
      //   await axios.get(`${host}/gdrive/getAccessToken`).then((res) => {
      //     window.open(res.data, "_self");
      //     console.log(res);
      //   });
      // };

      // fetchData();

      // try {
      //   window.open(
      //     "https://senti-a-ialert-website-pd.df.r.appspot.com/login",
      //     "_self"
      //   );
      // } catch {
      //   window.open("http://localhost:80/login", "_self");
      // }
    } else if (token !== "") {
      console.log("logged in");

      const fetchData = async () => {
        await axios.get(`${host}/gdrive/MyDrive`).then(function(response) {
          console.log("updated my drive");
          setFiles(response.data);
          window.open(`${host}/main`, "_self");
          // setPrevFiles(response.data);
        });
      };

      fetchData();

      const data = new FormData();

      data.append(0, JSON.stringify(token));
      // send token data to backend
      axios
        .post(`${host}/gdrive/postToken`, data)
        .then((res) => {
          console.log(res);
        })
        .catch((err) => console.log(err));
    }
  };

  return (
    <div id="loginModal" class="modal-opaque">
      <div class="modal-content-login">
        <div class="modal-input-body-cont2">
          <div>
            <img className="login-logo" src={logo} alt="logo"></img>
          </div>
          <div style={{ textAlign: "center", width: "100%", padding: "16px" }}>
            <h2>INTRANET</h2>
          </div>
          <button onClick={() => handleSignIn()}>SIGN IN WITH GOOGLE</button>
        </div>
      </div>
    </div>
  );
}
