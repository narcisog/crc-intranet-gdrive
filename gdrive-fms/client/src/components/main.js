import "../App.css";
import "./styles/components.css";
import React, { useState, useEffect, useContext } from "react";
import axios from "axios";
import Moment from "react-moment";
import logo from "../assets/FP-icon-CA-logo@2x-white.png";

import UploadBox from "./uploadBox";
import NewFolder from "./newFolder";
import PermissionsModal from "./permissionsModal";
import LoadingScreen from "./loading";
import ConfirmationScreen from "./confirmationScreen";
import { Context } from "../contexts/contexts";

export default function Main() {
  let locNav = JSON.parse(window.localStorage.getItem("nav"));
  const {
    fID,
    setFID,
    files,
    setFiles,
    setPrevFiles,
    setPrevDrive,
    drive,
    setDrive,
    forupload,
    setForupload,
    permissions,
    setPermissions,
    setFileName,
    setDomain,
    email,
    setEmail,
    setLoading,
    setMessage,
    setDeleteFile,
    setPrevPermissions,
  } = useContext(Context);

  // const host = "http://localhost:8080";
  const host = "https://git.heroku.com/crc-intranet.git";

  const [token, setToken] = useState("");
  const [nav, setNav] = useState(
    locNav === null ? [{ id: "0", name: "My Drive" }] : locNav
  );

  var uploadBox = document.getElementById("myModal");
  var newFolder = document.getElementById("myNewFolderModal");
  var permissionsModal = document.getElementById("permissions");
  var loadingScreen = document.getElementById("loadingScreen");
  var confirmationScreen = document.getElementById("confirmationScreen");

  const handleLogOut = () => {
    window.localStorage.setItem("isSignedIn", "false");
    window.localStorage.removeItem("token");
    window.localStorage.removeItem("nav");
    window.localStorage.removeItem("drive");
    window.open(`http://localhost:3000`, "_self");
  };

  const handleUpload = () => {
    setLoading("UPLOADING DOCUMENTS...");
    var permissionCont = permissions;
    var newPermissions = [];

    for (let i = 0; i < permissionCont.length; i++) {
      if (
        permissionCont[i].role === "owner" &&
        permissionCont[i].emailAddress !== email
      ) {
        newPermissions.push({
          type: permissionCont[i].type,
          role: "writer",
          emailAddress: permissionCont[i].emailAddress,
        });
      } else {
        newPermissions.push(permissionCont[i]);
      }
    }
    setPermissions(newPermissions);
    uploadBox.style.display = "flex";
  };

  const handleNewFolder = () => {
    handleDrive("newFile", "");
    setLoading("CREATING FOLDER...");
    newFolder.style.display = "flex";
  };

  const getPermissions = async (id) => {
    await axios
      .get(`${host}/gdrive/getPermissions`, {
        params: {
          fID: id,
        },
      })
      .then((res) => {
        setPermissions(res.data);
      })
      .catch((err) => console.log("permissions were not retrieved"));
  };

  const handlePermissionModal = async (e) => {
    setPrevPermissions(permissions);

    e.permissions === undefined
      ? setLoading("UNABLE TO RETRIEVE PERMISSIONS.")
      : setLoading("FETCHING DATA!");

    if (e.permissions !== undefined) {
      loadingScreen.style.display = "flex";
      await axios
        .get(`${host}/gdrive/getPermissions`, {
          params: {
            fID: e.id,
          },
        })
        .then((res) => {
          setPermissions(res.data);
          setFileName(e.name);
          setFID(e.id);
          loadingScreen.style.display = "none";
          permissionsModal.style.display = "flex";
        })
        .catch((err) => console.log("permissions were not retrieved"));
    } else if (e.permissions === undefined) {
      loadingScreen.style.display = "flex";

      setTimeout(function() {
        loadingScreen.style.display = "none";
      }, 1500);
    }
  };

  const handleDrive = (name, folder) => {
    if (name === "myDrive") {
      setNav([{ id: "0", name: "My Drive" }]);
      console.log(fID, "Location:", drive);
      setFID("");
      setDrive("myDrive");
      setPermissions([]);
      loadingScreen.style.display = "flex";
      const fetchData = async () => {
        await axios.get(`${host}/gdrive/MyDrive`).then(function(response) {
          setFiles(response.data);
          loadingScreen.style.display = "none";
        });
      };

      fetchData();
    } else if (name === "shared") {
      setNav([{ id: "1", name: "Shared Folders" }]);
      setFID("");
      setDrive("shared");
      setPermissions([]);
      loadingScreen.style.display = "flex";
      const fetchData = async () => {
        await axios.get(`${host}/gdrive/Shared`).then(function(response) {
          setFiles(response.data);
          loadingScreen.style.display = "none";
        });
      };

      fetchData();
    } else if (name === "crc") {
      setNav([{ id: "2", name: "Conflict Resource Center" }]);
      setFID("");
      setDrive("crc");
      setPermissions([]);
      loadingScreen.style.display = "flex";
      const fetchData = async () => {
        await axios.get(`${host}/gdrive/crc`).then(function(response) {
          setFiles(response.data);
          setPrevFiles(response.data);
          loadingScreen.style.display = "none";
        });
      };

      fetchData();
    } else if (name === "specificFolder") {
      loadingScreen.style.display = "flex";
      const fetchData = async () => {
        await axios
          .get(`${host}/gdrive/folder`, {
            params: { fID: folder.id },
          })
          .then(function(response) {
            let index = nav.indexOf(folder);
            if (index !== -1) {
              var arr = [];
              for (let i = 0; i <= index; i++) {
                arr.push(nav[i]);
              }
              setNav(arr);
            } else if (index === -1) {
              setNav([...nav, { id: folder.id, name: folder.name }]);
            }
            setDrive("specificFolder");
            setFID(folder.id);

            getPermissions(folder.id);

            setFiles(response.data);
            setPrevFiles(response.data);
            loadingScreen.style.display = "none";
          })
          .catch((err) => console.log("Error fetching data!", err));
      };

      fetchData();
    } else if (name === "newFile") {
      setDrive("newFile");
      setPrevDrive(drive);
    } else if (name === "uploadFile") {
      setDrive("uploadFile");
      setPrevDrive(drive);
    } else if (name === "delete") {
      setDrive("delete");
      setPrevDrive(drive);
    }
  };

  const handleFileChange = (e) => {
    e.preventDefault();
    setForupload({ ...forupload, fID: fID, fileObject: e.target.files });

    if (e.target.files.length !== 0) {
      handleUpload();
    }
  };

  const generateURL = (e) => {
    const getUrl = async () => {
      await axios
        .get(`${host}/gdrive/generateURL`, {
          params: {
            fID: e,
          },
        })
        .then((res) => {
          window.open(res.data["webViewLink"]);
        })
        .catch((err) => console.log(err));
    };

    getUrl();
  };

  const handleDeleteFile = (e) => {
    handleDrive("delete", "");
    setDeleteFile(e);
    console.log(e);
    setLoading(`DELETING FILE: ${e.name}`);
    setMessage(`DELETE FILE: ${e.name}?`);
    confirmationScreen.style.display = "flex";
  };

  useEffect(() => {
    window.localStorage.setItem("nav", JSON.stringify(nav));
    window.localStorage.setItem("drive", drive);
    window.localStorage.setItem("fID", fID);
    if (token !== "" || token !== null || token !== undefined) {
      window.localStorage.setItem("token", JSON.stringify(token));
    }
  });

  useEffect(() => {
    const getToken = async () => {
      await axios
        .get(`${host}/gdrive/getToken`)
        .then(async (res) => {
          if (res.data !== undefined || res.data !== null) {
            await axios
              .get(
                `https://oauth2.googleapis.com/tokeninfo?id_token=${res.data.id_token}`
              )
              .then((res) => {
                setDomain(res.data.email.split("@")[1]);
                setEmail(res.data.email);
              })
              .catch((err) => console.log(err));
            setToken(res.data);
          } else if (res.data === undefined || res.data === null) {
            const data = new FormData();

            var tok = window.localStorage.getItem("token");
            data.append(0, tok);
            await axios
              .post(`${host}/gdrive/postToken`, data)
              .catch((err) => console.log(err));
          }
        })
        .catch((err) => console.log(err));
    };

    getToken();
  }, [setFiles, drive, fID]);

  const getData = () => {
    let lastFolder = JSON.parse(window.localStorage.getItem("nav"));
    if (files.length === 0) {
      if (lastFolder[lastFolder.length - 1].id === "0") {
        const fetchData = async () => {
          await axios.get(`${host}/gdrive/MyDrive`).then(function(response) {
            setFiles(response.data);
          });
        };

        fetchData();
      } else if (lastFolder[lastFolder.length - 1].id === "1") {
        const fetchData = async () => {
          await axios.get(`${host}/gdrive/Shared`).then(function(response) {
            setFiles(response.data);
          });
        };

        fetchData();
      } else if (lastFolder[lastFolder.length - 1].id === "2") {
        const fetchData = async () => {
          await axios.get(`${host}/gdrive/crc`).then(function(response) {
            setFiles(response.data);
            setPrevFiles(response.data);
          });
        };

        fetchData();
      } else {
        const fetchData = async () => {
          await axios
            .get(`${host}/gdrive/folder`, {
              params: { fID: lastFolder[lastFolder.length - 1].id },
            })
            .then(function(response) {
              setDrive("specificFolder");
              setFID(lastFolder[lastFolder.length - 1].id);

              getPermissions(lastFolder[lastFolder.length - 1].id);

              setFiles(response.data);
              setPrevFiles(response.data);
            })
            .catch((err) => console.log("Error fetching data!", err));
        };

        fetchData();
      }
    }
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <div className="App">
      <div className="sidePanel">
        <div className="panel">
          <LoadingScreen />
          <ConfirmationScreen />
          <div className="panelRow-logo">
            <img className="main-logo" src={logo} alt="logo"></img>
          </div>
          <div className="panelRow-heading">
            <h3>INTRANET</h3>
          </div>

          <div className="panelRow">
            <hr></hr>
          </div>

          <div className="panelRow">
            <input
              className="panel-button"
              type="submit"
              value="CONFLICT RESOURCE CENTER"
              name="crc"
              onClick={(e) => handleDrive(e.target.name, "")}
            ></input>
          </div>

          <div className="panelRow">
            <hr></hr>
          </div>

          <div className="panelRow">
            <input
              className="panel-button"
              type="submit"
              value="MY DRIVE"
              name="myDrive"
              onClick={(e) => handleDrive(e.target.name, "")}
            ></input>
          </div>

          <div className="panelRow">
            <hr></hr>
          </div>

          <div className="panelRow">
            <input
              className="panel-button"
              type="submit"
              value="SHARED FOLDERS"
              name="shared"
              onClick={(e) => handleDrive(e.target.name, "")}
            ></input>
          </div>

          <div className="panelRow">
            <hr></hr>
          </div>

          <div className="panelRow">
            <input
              className="panel-button"
              disabled={drive === "shared" || drive === "crc"}
              type="submit"
              value="NEW FOLDER"
              onClick={() => handleNewFolder()}
            ></input>
            <NewFolder />
          </div>

          <div className="panelRow">
            <hr></hr>
          </div>

          <div className="panelRow">
            <input
              disabled={drive === "shared" || drive === "crc"}
              id="fileInput"
              className="inputfile"
              type="file"
              name="file"
              multiple
              onChange={(e) => handleFileChange(e)}
            ></input>
            <label for="file">UPLOAD</label>
            <UploadBox />
          </div>

          <div className="panelRow">
            <hr></hr>
          </div>
        </div>

        <div className="panel"></div>
      </div>
      <div className="mainContainer">
        <div className="header-1">
          <button className="logOut-button" onClick={() => handleLogOut()}>
            LOGOUT
          </button>
        </div>
        <div className="header-2">
          <div className="nav">
            {nav.map((item) => (
              <button
                className="nav-button"
                value={item.id}
                onClick={() => {
                  if (item.id === "0") {
                    handleDrive("myDrive", item);
                  } else if (item.id === "1") {
                    handleDrive("shared", item);
                  } else {
                    handleDrive("specificFolder", item);
                  }
                }}
              >
                {item.name} &#62;
              </button>
            ))}
          </div>
        </div>
        <table className="mainTable">
          <thead>
            <tr>
              <th>Name</th>
              <th>Access</th>
              <th>File type</th>
              <th>Last Modified</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {files.map((row) => {
              return (
                <tr
                  key={row.id}
                  id="document_row"
                  name="specificFolder"
                  class="fileRow"
                  value={row.id}
                >
                  <td
                    id="doc_name"
                    className="td1"
                    onDoubleClick={
                      row.mimeType.split(".").at(-1) !== "folder"
                        ? null
                        : () => handleDrive("specificFolder", row)
                    }
                  >
                    {row.name}
                  </td>
                  <td className="td1">
                    {"permissions" in row
                      ? row.permissions.filter(
                          (item) => item.emailAddress === email
                        )[0] !== undefined
                        ? row.permissions.filter(
                            (item) => item.emailAddress === email
                          )[0].role
                        : row.permissions.filter(
                            (item) => item.id === "anyoneWithLink"
                          )[0].role
                      : null}
                  </td>
                  <td className="td1">{row.mimeType.split(".").at(-1)}</td>
                  <td className="td1">
                    <Moment format="YYYY-MM-DD">{row.modifiedTime}</Moment>
                  </td>
                  <td className="td1">
                    <div class="actions">
                      <button>
                        <i
                          id="download"
                          class="bi bi-download"
                          style={{ fontSize: "20px" }}
                          onClick={() => generateURL(row.id)}
                        ></i>
                      </button>
                      <button>
                        <i
                          id="settings"
                          class="bi bi-gear settings"
                          style={{ fontSize: "20px" }}
                          onClick={(e) =>
                            e.target.id === "settings"
                              ? handlePermissionModal(row)
                              : null
                          }
                        ></i>
                      </button>
                      <PermissionsModal />
                      <button>
                        <i
                          id="delete"
                          class="bi bi-x-square"
                          style={{ fontSize: "20px" }}
                          onClick={() => handleDeleteFile(row)}
                        ></i>
                      </button>
                    </div>
                  </td>
                </tr>
              );
            })}
          </tbody>
          <tfoot>
            <tr></tr>
          </tfoot>
        </table>
      </div>
    </div>
  );
}
