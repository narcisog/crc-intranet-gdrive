import React, { useState, useContext } from "react";
import "./styles/components.css";
import axios from "axios";

import { Context } from "../contexts/contexts";

export default function NewFolder() {
  const {
    fID,
    email,
    setDrive,
    prevDrive,
    setFiles,
    setFID,
    setLoading,
    loading,
    permissions,
    setPermissions,
  } = useContext(Context);
  const [folder, setFolder] = useState({
    title: "New Folder",
    folderID: fID,
  });
  // const [permissions, setPermissions] = useState([]);
  const [permission, setPermission] = useState({
    type: "user",
    role: "reader",
    emailAddress: "",
  });

  // const host = "http://localhost:8080";
  const host = "https://git.heroku.com/crc-intranet.git";

  var modal = document.getElementById("myNewFolderModal");
  var loadingWidget = document.getElementById("loadingWidget");

  const handlePermission = (e) => {
    e.preventDefault();
    setPermission({
      ...permission,
      type: "user",
      [e.target.name]: [e.target.value],
    });
  };

  const handleAddPermissions = () => {
    console.log(permission);
    setPermissions([...permissions, permission]);
    setPermission({
      type: "user",
      role: "reader",
      emailAddress: "",
    });
  };

  const handleRemove = (e) => {
    var arr = [];

    for (let i = 0; i < permissions.length; i++) {
      if (i !== e) {
        arr.push(permissions[i]);
      }
    }

    setPermissions(arr);
  };

  const handleInputChange = (e) => {
    setFolder({ ...folder, title: e.target.value, folderID: fID });
  };

  const handleCreateFolder = () => {
    loadingWidget.style.display = "flex";
    const data = new FormData();

    const newPermissions = permissions
      .filter((item) => item.emailAddress !== email)
      .map(({ type, role, emailAddress }) => ({ type, role, emailAddress }));

    const postData = async () => {
      await axios
        .get(`${host}/gdrive/create`, {
          params: {
            title: folder.title,
            fID: folder.folderID,
          },
        })
        .then((res) => {
          console.log("RESPONSE", res.data);
          data.append(0, res.data);
          data.append("p", JSON.stringify(newPermissions));
          setLoading("FOLDER HAS BEEN CREATED!");

          setTimeout(function() {
            handleClose();
            loadingWidget.style.display = "none";
            setLoading("FETCHING DATA!");
          }, 1500);

          const postPermissions = async () => {
            await axios
              .post(`${host}/gdrive/share`, data)
              .then(() => {
                console.log("PERMISSIONS CREATED! ", res);
              })
              .catch(console.log((err) => console.log(err)));
          };

          postPermissions();
          setDrive(prevDrive);
          // setPermissions([]);
        });
    };

    postData();
    console.log(folder);
  };

  const handleClose = () => {
    setLoading("FETCHING DATA!");
    modal.style.display = "none";
    setFolder({ ...folder, title: "New folder", fID: folder.folderID });
    let lastFolder = JSON.parse(window.localStorage.getItem("nav"));

    if (lastFolder[lastFolder.length - 1].id === "0") {
      const fetchData = async () => {
        await axios.get(`${host}/gdrive/MyDrive`).then(function(response) {
          console.log("updated my drive");
          setFiles(response.data);
          modal.style.display = "none";
        });
      };

      fetchData();
    } else if (lastFolder[lastFolder.length - 1].id === "1") {
      const fetchData = async () => {
        await axios.get(`${host}/gdrive/Shared`).then(function(response) {
          console.log("updated my shared drive");
          setFiles(response.data);
          modal.style.display = "none";
        });
      };

      fetchData();
    } else if (lastFolder[lastFolder.length - 1].id === "2") {
      const fetchData = async () => {
        await axios.get(`${host}/gdrive/crc`).then(function(response) {
          console.log("updated crc");
          setFiles(response.data);
          modal.style.display = "none";
        });
      };

      fetchData();
    } else {
      const fetchData = async () => {
        console.log(
          "FETCHING DATA!",
          "FID",
          fID,
          "FOLDER ID",
          lastFolder[lastFolder.length - 1].id
        );
        await axios
          .get(`${host}gdrive/folder`, {
            params: { fID: lastFolder[lastFolder.length - 1].id },
          })
          .then(function(response) {
            console.log("updated folder");
            setDrive("specificFolder");
            setFID(lastFolder[lastFolder.length - 1].id);

            setFiles(response.data);

            modal.style.display = "none";
          })
          .catch((err) => console.log("Error fetching data!", err));
      };

      fetchData();
    }
    console.log(prevDrive);
  };

  return (
    <div
      id="myNewFolderModal"
      class="modal"
      onClick={(e) =>
        e.target === document.getElementById("myNewFolderModal")
          ? handleClose()
          : null
      }
    >
      <div class="modal-content">
        <div class="modal-header">
          <span class="close">
            <i
              id="delete"
              class="bi bi-x-square"
              style={{ fontSize: "20px" }}
              onClick={() => handleClose()}
            ></i>
          </span>
          <h2>Create New Folder</h2>
        </div>
        <div class="modal-input-body">
          <input
            type="text"
            class="create-folder"
            value={folder.title}
            onChange={(e) => handleInputChange(e)}
          ></input>
        </div>
        <div class="modal-input-body-cont2">
          <div class="access-cont">
            <h4>PERMISSIONS</h4>
            <div class="modal-input-body-cont-row">
              <input
                class="email-input"
                type="email"
                name="emailAddress"
                value={permission.emailAddress}
                onChange={(e) => handlePermission(e)}
              ></input>
              <select
                name="role"
                id="roles2"
                value={permission.role}
                onChange={(e) => handlePermission(e)}
              >
                <option value="organizer/owner">Organizer</option>
                <option value="fileOrganizer">File Organizer</option>
                <option value="writer">Writer</option>
                <option value="reader">Reader</option>
              </select>
              <input
                class="permissions-button"
                type="submit"
                value="ADD"
                onClick={() => handleAddPermissions()}
              ></input>
            </div>

            <div class="access-sub-cont-scroll">
              {permissions.map((item, index) => {
                return (
                  <div class="options-cont">
                    <p class="options-emailAdd">{item.emailAddress}</p>
                    <p class="options-role">{item.role}</p>
                    <p class="options-del" onClick={() => handleRemove(index)}>
                      <i
                        class="bi bi-x-square"
                        style={{ fontSize: "15px", color: "black" }}
                      ></i>
                    </p>
                  </div>
                );
              })}
            </div>
          </div>
        </div>
        <div id="loadingWidget">
          <p>{loading}</p>
        </div>
        <div class="modal-footer">
          <input
            type="submit"
            className="button"
            value="CREATE NEW FOLDER"
            onClick={() => handleCreateFolder()}
          ></input>
        </div>
      </div>
    </div>
  );
}
