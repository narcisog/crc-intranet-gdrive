import React, { useContext } from "react";
import "./styles/components.css";

import { Context } from "../contexts/contexts";

export default function LoadingScreen() {
  const { loading } = useContext(Context);

  return (
    <div id="loadingScreen" class="modal">
      <div class="modal-content-buffer">
        <div>
          <p>{loading}</p>
        </div>
      </div>
    </div>
  );
}
