import React, { useState, useContext } from "react";
import "./styles/components.css";
import axios from "axios";

import { Context } from "../contexts/contexts";

export default function PermissionsModal() {
  const {
    fID,
    prevDrive,
    permissions,
    setPermissions,
    fileName,
    prevPermissions,
  } = useContext(Context);
  const [forDelete, setForDelete] = useState([]);
  const [forUpdate, setForUpdate] = useState([]);
  const [newPermissions, setNewPermissions] = useState([]);
  const [permission, setPermission] = useState({
    type: "user",
    role: "reader",
    emailAddress: "",
  });

  // const host = "http://localhost:8080";
  const host = "https://git.heroku.com/crc-intranet.git";

  var permissionsModal = document.getElementById("permissions");

  const handlePermission = (e) => {
    e.preventDefault();
    setPermission({
      ...permission,
      type: "user",
      [e.target.name]: e.target.value,
    });
  };

  const handleAddPermissions = () => {
    console.log(permission);
    setNewPermissions([...newPermissions, permission]);
    setPermissions([...permissions, permission]);
    setPermission({
      type: "user",
      role: "reader",
      emailAddress: "",
    });
  };

  const handleUpdateRole = (e, val, email) => {
    if (val !== undefined) {
      setForUpdate([
        ...forUpdate.filter((e) => e.id !== val),
        {
          id: val,
          emailAddress: email,
          type: "user",
          role: e.target.value,
        },
      ]);
    } else if (val === undefined) {
      setNewPermissions([
        ...newPermissions.filter((item) => item.emailAddress !== email),
        {
          type: "user",
          role: e.target.value,
          emailAddress: email,
        },
      ]);
      setForUpdate([
        ...forUpdate.filter((item) => item.emailAddress !== email),
        {
          id: "",
          emailAddress: email,
          type: "user",
          role: e.target.value,
        },
      ]);
    }
  };

  const handleRemove = (id, email) => {
    setForDelete([
      ...forDelete,
      permissions.filter(
        (item) => item.id === id && item.emailAddress === email
      ),
    ]);

    setPermissions([
      ...permissions.filter(
        (item) => item.id !== id && item.emailAddress !== email
      ),
    ]);

    setNewPermissions([
      ...newPermissions.filter((item) => item.emailAddress !== email),
    ]);
  };

  const handleUpdate = () => {
    if (newPermissions.length > 0) {
      const data = new FormData();
      data.append(0, fID);
      data.append("p", JSON.stringify(newPermissions));

      const postPermissions = async () => {
        await axios
          .post(`${host}/gdrive/share`, data)
          .then((res) => {
            console.log("drive", prevDrive);
            handleClose();
          })
          .catch((err) => console.log(err));
      };

      postPermissions();
    } else if (newPermissions.length === 0) {
      handleClose();
    }

    if (forDelete.length > 0) {
      const deletePermissions = async () => {
        await axios
          .delete(`${host}/gdrive/deletePermissions`, {
            params: {
              fID: fID,
              forDelete: JSON.stringify(forDelete),
            },
          })
          .then((res) => {
            console.log("DELETED!", res);
            handleClose();
          })
          .catch((err) => console.log(err));
      };

      deletePermissions();
    }

    if (forUpdate.length > 0) {
      const data = new FormData();
      data.append(0, fID);
      data.append("p", JSON.stringify(forUpdate.filter((e) => e.id !== "")));
      const updatePermissions = async () => {
        await axios
          .put(`${host}/gdrive/updatePermissions`, data)
          .then((res) => {
            console.log("UPDATED!", res);
            handleClose();
          })
          .catch((err) => console.log(err));
      };

      updatePermissions();
      console.log(forUpdate.filter((e) => e.id !== ""));
    }

    console.log("FOR DELETE", forDelete);
    console.log("FOR UPDATE", forUpdate);
    console.log("NEW PERMISSIONS", newPermissions);
  };

  // const getPermissions = async (id) => {
  //   await axios
  //     .get(`${host}/gdrive/getPermissions`, {
  //       params: {
  //         fID: id,
  //       },
  //     })
  //     .then((res) => {
  //       setPermissions(res.data);
  //       console.log(drive, res.data);
  //       permissionsModal.style.display = "none";
  //     })
  //     .catch((err) => {
  //       console.log("permissions were not retrieved");
  //       permissionsModal.style.display = "none";
  //     });
  // };

  const handleClose = () => {
    setPermissions(prevPermissions);
    setForDelete([]);
    setForUpdate([]);
    setNewPermissions([]);
    permissionsModal.style.display = "none";
  };

  return (
    <div
      id="permissions"
      class="modal"
      onClick={(e) => (e.target === permissionsModal ? handleClose() : null)}
    >
      <div class="modal-content">
        <div class="modal-header">
          <span class="close">
            <i
              id="delete"
              class="bi bi-x-square"
              style={{ fontSize: "20px" }}
              onClick={() => handleClose()}
            ></i>
          </span>
          <h2>Permissions for "{fileName}"</h2>
        </div>

        <div class="modal-input-body-cont2">
          <div class="modal-input-body-cont-row">
            <input
              class="email-input"
              type="email"
              name="emailAddress"
              value={permission.emailAddress}
              onChange={(e) => handlePermission(e)}
            ></input>
            <select
              name="role"
              id="roles2"
              value={permission.role}
              onChange={(e) => handlePermission(e)}
            >
              <option value="organizer/owner">Organizer</option>
              <option value="fileOrganizer">File Organizer</option>
              <option value="writer">Writer</option>
              <option value="reader">Reader</option>
            </select>
            <input
              disabled={permission.emailAddress === "" ? true : false}
              class="permissions-button"
              type="submit"
              value="ADD"
              onClick={() => handleAddPermissions()}
            ></input>
          </div>

          <div class="access-sub-cont-scroll">
            {permissions.map((item, index) => {
              return (
                <div class="options-cont">
                  <p class="options-emailAdd">
                    {item.emailAddress !== undefined
                      ? item.displayName
                      : "Anyone with link"}{" "}
                    &nbsp; - {item.emailAddress}
                  </p>
                  <select
                    disabled={forDelete.indexOf(item.id) !== -1 ? true : false}
                    name="role"
                    class="options-role-select"
                    defaultValue={item.role}
                    value={
                      forUpdate.filter(
                        (e) => e.emailAddress === item.emailAddress
                      ).length > 0
                        ? forUpdate.filter(
                            (e) => e.emailAddress === item.emailAddress
                          ).role
                        : item.role
                    }
                    onChange={(e) =>
                      handleUpdateRole(e, item.id, item.emailAddress)
                    }
                  >
                    <option value="organizer/owner">Organizer</option>
                    <option value="fileOrganizer">File Organizer</option>
                    <option value="writer">Writer</option>
                    <option value="reader">Reader</option>
                  </select>
                  <p
                    class="options-del"
                    onClick={() => handleRemove(item.id, item.emailAddress)}
                  >
                    <i
                      class="bi bi-x-square"
                      style={{ fontSize: "15px", color: "black" }}
                    ></i>
                  </p>
                </div>
              );
            })}
          </div>
        </div>
        <div class="modal-footer">
          <input
            type="submit"
            className="button"
            value="UPDATE"
            onClick={() => handleUpdate()}
          ></input>
        </div>
      </div>
    </div>
  );
}
