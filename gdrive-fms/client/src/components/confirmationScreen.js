import React, { useContext } from "react";
import "./styles/components.css";
import axios from "axios";

import { Context } from "../contexts/contexts";

export default function ConfirmationScreen() {
  const {
    setLoading,
    message,
    deleteFile,
    prevDrive,
    setPrevDrive,
    drive,
    setDrive,
    setFiles,
    setPrevFiles,
    fID,
    setFID,
    files,
    setPermissions,
  } = useContext(Context);

  // const host = "http://localhost:8080";
  const host = "https://git.heroku.com/crc-intranet.git";

  var loadingScreen = document.getElementById("loadingScreen");
  var confirmationScreen = document.getElementById("confirmationScreen");

  const getPermissions = async (id) => {
    await axios
      .get(`${host}/gdrive/getPermissions`, {
        params: {
          fID: id,
        },
      })
      .then((res) => {
        // console.log("Original Permissions", res.data);
        console.log("Permissions retrieved", res.data);
        setPermissions(res.data);
      })
      .catch((err) => console.log("permissions were not retrieved"));
  };

  const getData = () => {
    let lastFolder = JSON.parse(window.localStorage.getItem("nav"));

    console.log("NAV", lastFolder[lastFolder.length - 1]);

    if (files.length === 0) {
      if (lastFolder[lastFolder.length - 1].id === "0") {
        const fetchData = async () => {
          await axios.get(`${host}/gdrive/MyDrive`).then(function(response) {
            console.log("updated my drive");
            setFiles(response.data);
            // setPrevFiles(response.data);
          });
        };

        fetchData();
      } else if (lastFolder[lastFolder.length - 1].id === "1") {
        const fetchData = async () => {
          await axios.get(`${host}/gdrive/Shared`).then(function(response) {
            console.log("updated my shared drive");
            setFiles(response.data);
            // setPrevFiles(response.data);
          });
        };

        fetchData();
      } else if (lastFolder[lastFolder.length - 1].id === "2") {
        const fetchData = async () => {
          await axios.get(`${host}/gdrive/crc`).then(function(response) {
            console.log("updated crc");
            setFiles(response.data);
            setPrevFiles(response.data);
          });
        };

        fetchData();
      } else {
        const fetchData = async () => {
          console.log(
            "FETCHING DATA!",
            "FID",
            fID,
            "FOLDER ID",
            lastFolder[lastFolder.length - 1].id
          );
          await axios
            .get(`${host}/gdrive/folder`, {
              params: { fID: lastFolder[lastFolder.length - 1].id },
            })
            .then(function(response) {
              console.log("updated folder");
              setDrive("specificFolder");
              setFID(lastFolder[lastFolder.length - 1].id);

              getPermissions(lastFolder[lastFolder.length - 1].id);

              setFiles(response.data);
              setPrevFiles(response.data);
            })
            .catch((err) => console.log("Error fetching data!", err));
        };

        fetchData();
      }
    }
  };

  const handleDelete = () => {
    confirmationScreen.style.display = "none";
    loadingScreen.style.display = "flex";
    const delFile = async () => {
      await axios
        .delete(`${host}/gdrive/delete`, {
          params: {
            fID: deleteFile.id,
          },
        })
        .then((res) => {
          setLoading("DOCUMENT DELETED!");
          setTimeout(() => {
            loadingScreen.style.display = "none";
            setLoading("FETCHING DATA!");
          }, 1000);
          getData();
          setFiles([...files.filter((item) => item.id !== deleteFile.id)]);
          console.log("DELETE", res);
        })
        .catch((err) => {
          setLoading(err);
          setTimeout(() => {
            loadingScreen.style.display = "none";
            setLoading("FETCHING DATA!");
          }, 1000);
          getData();
        });

      setDrive(prevDrive);
    };

    delFile();
    setPrevDrive(drive);
  };

  const cancelDelete = () => {
    setLoading("FETCHING DATA!");
    setDrive(prevDrive);
    confirmationScreen.style.display = "none";
  };

  return (
    <div id="confirmationScreen" class="modal">
      <div class="modal-content-buffer">
        <div className="modal-content-row">
          <p>{message}</p>
        </div>
        <div className="modal-content-row">
          <div>
            <button onClick={() => handleDelete()}>YES</button>
          </div>

          <div>
            <button onClick={() => cancelDelete()}>CANCEL</button>
          </div>
        </div>
      </div>
    </div>
  );
}
