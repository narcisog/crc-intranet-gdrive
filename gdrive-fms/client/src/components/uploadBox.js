import React, { useState, useContext } from "react";
import "./styles/components.css";
import axios from "axios";

import { Context } from "../contexts/contexts";
export default function UploadBox() {
  const {
    forupload,
    setForupload,
    setDrive,
    prevDrive,
    fID,
    domain,
    permissions,
    setPermissions,
    email,
    setFiles,
    setFID,
    loading,
    setLoading,
  } = useContext(Context);
  const [permission, setPermission] = useState({
    type: "user",
    role: "reader",
    emailAddress: "",
  });

  // const host = "http://localhost:8080";
  const host = "https://git.heroku.com/crc-intranet.git";

  var modal = document.getElementById("myModal");
  var loadingWidget = document.getElementById("loadingWidget2");
  var fileInput = document.getElementById("fileInput");

  const handlePermission = (e) => {
    e.preventDefault();
    setPermission({
      ...permission,
      type: "user",
      [e.target.name]: [e.target.value],
    });
  };

  const handleAddPermissions = () => {
    console.log(permission);
    setPermissions([...permissions, permission]);
    setPermission({
      type: "user",
      role: "reader",
      emailAddress: "",
    });
  };

  const handleRemove = (e) => {
    var arr = [];

    for (let i = 0; i < permissions.length; i++) {
      if (i !== e) {
        arr.push(permissions[i]);
      }
    }

    setPermissions(arr);
  };

  const handleUpload = () => {
    loadingWidget.style.display = "flex";

    const uploadData = async () => {
      const data = new FormData();

      data.append(0, fID);
      data.append(
        "p",
        JSON.stringify(
          permissions.filter((item) => item.emailAddress !== email)
        )
      );
      data.append("domain", domain);

      for (let i = 0; i < forupload.fileObject.length; i++) {
        data.append(i, forupload.fileObject[i]);
      }

      await axios
        .post(`${host}/gdrive/upload`, data)
        .then((res) => {
          console.log("Files uploaded!", res.data);
          setDrive(prevDrive);
          setPermissions([]);

          setLoading("UPLOADED!");

          setTimeout(function() {
            handleClose();
            loadingWidget.style.display = "none";
            setLoading("Fetching Data!");
          }, 1000);
        })
        .catch((err) => {
          console.log("Failed to upload!", err);
          if (err.response.status === 401) {
            setLoading("Edit access is not granted to your account.");
          }
          setPermissions([]);

          setTimeout(function() {
            handleClose();
            loadingWidget.style.display = "none";
            setLoading("Fetching Data!");
          }, 1000);
        });
    };

    uploadData();
  };

  const handleClose = () => {
    fileInput.value = "";
    // setPermissions([]);
    setForupload({
      fID: fID,
      fileObject: {},
    });

    let lastFolder = JSON.parse(window.localStorage.getItem("nav"));

    if (lastFolder[lastFolder.length - 1].id === "0") {
      const fetchData = async () => {
        await axios.get(`${host}/gdrive/MyDrive`).then(function(response) {
          console.log("updated my drive");
          setFiles(response.data);
          modal.style.display = "none";
        });
      };

      fetchData();
    } else if (lastFolder[lastFolder.length - 1].id === "1") {
      const fetchData = async () => {
        await axios.get(`${host}/gdrive/Shared`).then(function(response) {
          console.log("updated my shared drive");
          setFiles(response.data);
          modal.style.display = "none";
        });
      };

      fetchData();
    } else if (lastFolder[lastFolder.length - 1].id === "2") {
      const fetchData = async () => {
        await axios.get(`${host}/gdrive/crc`).then(function(response) {
          console.log("updated crc");
          setFiles(response.data);
          modal.style.display = "none";
        });
      };

      fetchData();
    } else {
      const fetchData = async () => {
        console.log(
          "FETCHING DATA!",
          "FID",
          fID,
          "FOLDER ID",
          lastFolder[lastFolder.length - 1].id
        );
        await axios
          .get(`${host}/gdrive/folder`, {
            params: { fID: lastFolder[lastFolder.length - 1].id },
          })
          .then(function(response) {
            console.log("updated folder");
            setDrive("specificFolder");
            setFID(lastFolder[lastFolder.length - 1].id);

            setFiles(response.data);

            modal.style.display = "none";
          })
          .catch((err) => console.log("Error fetching data!", err));
      };

      fetchData();
    }
    console.log(typeof forupload.fileObject);
  };

  return (
    <div
      id="myModal"
      class="modal"
      onClick={(e) =>
        e.target === document.getElementById("myModal") ? handleClose() : null
      }
    >
      <div class="modal-content uploadBox">
        <div class="modal-header">
          <span class="close">
            <i
              id="delete"
              class="bi bi-x-square"
              style={{ fontSize: "20px" }}
              onClick={() => handleClose()}
            ></i>
          </span>
          <h2>UPLOAD FILES</h2>
        </div>
        <div class="modal-input-body">
          <div class="modal-input-body-cont">
            {Object.entries(forupload.fileObject).map((file, index) => {
              return (
                <div class="files-cont">
                  <p class="fileName">{file[1].name}</p>
                  <p class="fileType">
                    {
                      file[1].name.split(".")[
                        file[1].name.split(".").length - 1
                      ]
                    }
                  </p>
                </div>
              );
            })}
          </div>
          <div class="modal-input-body-cont2">
            <h4>PERMISSIONS</h4>
            <div class="modal-input-body-cont-row">
              <input
                class="email-input"
                type="email"
                name="emailAddress"
                value={permission.emailAddress}
                onChange={(e) => handlePermission(e)}
              ></input>
              <select
                name="role"
                id="roles"
                value={permission.role}
                onChange={(e) => handlePermission(e)}
              >
                <option value="organizer/owner">Organizer</option>
                <option value="fileOrganizer">File Organizer</option>
                <option value="writer">Writer</option>
                <option value="reader">Reader</option>
              </select>
              <input
                class="permissions-button"
                type="submit"
                value="ADD"
                onClick={() => handleAddPermissions()}
              ></input>
            </div>

            <div class="access-sub-cont-scroll">
              {permissions.map((item, index) => {
                return (
                  <div class="options-cont">
                    <p class="options-emailAdd">{item.emailAddress}</p>
                    <p class="options-role">{item.role}</p>
                    <p class="options-del" onClick={() => handleRemove(index)}>
                      <i
                        class="bi bi-x-square"
                        style={{ fontSize: "15px", color: "black" }}
                      ></i>
                    </p>
                  </div>
                );
              })}
            </div>
          </div>
        </div>
        <div id="loadingWidget2">
          <p>{loading}</p>
        </div>
        <div class="modal-footer">
          <input
            id="fileInput"
            type="submit"
            class="button"
            value="UPLOAD"
            onClick={() => handleUpload()}
          ></input>
        </div>
      </div>
    </div>
  );
}
