import React, { createContext, useState } from "react";

export const Context = createContext();

const ContextProvider = (props) => {
  let locDrive = window.localStorage.getItem("drive");
  if (locDrive === null) {
    window.localStorage.setItem("drive", "myDrive");
    locDrive = "myDrive";
  }
  let locfID = window.localStorage.getItem("fID");
  const client_id =
    "611873017973-04maitp5l9mrutflsuuee3m0c2i81cso.apps.googleusercontent.com";
  const SCOPES = [
    "https://www.googleapis.com/auth/drive",
    "https://www.googleapis.com/auth/openid",
    "https://www.googleapis.com/auth/userinfo.email",
    "https://www.googleapis.com/auth/userinfo.profile",
  ];
  const [fID, setFID] = useState(locfID);
  const [files, setFiles] = useState([]);
  const [prevFiles, setPrevFiles] = useState([]);
  const [prevDrive, setPrevDrive] = useState("");
  const [drive, setDrive] = useState(locDrive);
  const [forupload, setForupload] = useState({
    fID: fID,
    fileObject: {},
  });
  const [permissions, setPermissions] = useState([]);
  const [fileName, setFileName] = useState("");
  const [user, setUser] = useState({
    googleId: "",
    email: "",
  });
  const [isSignedIn, setIsSignedIn] = useState(false);
  const [domain, setDomain] = useState("");
  const [email, setEmail] = useState("");
  const [loading, setLoading] = useState("FETCHING DATA!");
  const [message, setMessage] = useState("");
  const [deleteFile, setDeleteFile] = useState({});
  const [prevPermissions, setPrevPermissions] = useState([]);

  return (
    <Context.Provider
      value={{
        client_id,
        SCOPES,
        fID,
        setFID,
        files,
        setFiles,
        prevFiles,
        setPrevFiles,
        prevDrive,
        setPrevDrive,
        drive,
        setDrive,
        forupload,
        setForupload,
        permissions,
        setPermissions,
        fileName,
        setFileName,
        user,
        setUser,
        isSignedIn,
        setIsSignedIn,
        domain,
        setDomain,
        email,
        setEmail,
        loading,
        setLoading,
        message,
        setMessage,
        deleteFile,
        setDeleteFile,
        prevPermissions,
        setPrevPermissions,
      }}
    >
      {props.children}
    </Context.Provider>
  );
};

export default ContextProvider;
